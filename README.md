# Учебная практика. 3 курс

Краткая информация о практике: Учебная практика (практика по получению первичных профессиональных умений и навыков)

**Сроки практики**: 3 курс (6 семестр), 02.02.2021 — 15.02.2021

**Руководитель практики**: Шалденкова Анна Владимировна, кандидат ф.-м. наук, доцент кафедры ИТиЭО

**Автор портфолио**: Косыгин Кирилл Сергеевич, студент 3ИВТ1.1

## Документы

* [Отчёт](./Reports/Отчёт.pdf)

* [Задание](./Reports/Задание.pdf)

## Инвариативные задания

1.1. [Изучить и проанализировать печатные и Internet-источники по философским проблемам информатики.](./1.1/1.1.md)

1.2. [Выделить важные этапы в истории развития информатики и их социальные последствия.](./1.2/1.2.md)

1.3. [Изучить стандарты и спецификации в сфере ИТ.](./1.3/1.3.md)

1.4. [Изучить и освоить комплекс физических упражнений для программиста.](./1.4/1.4.md)

1.5. [Изучить и освоить гимнастику для глаз.](./1.5/1.5.md)

1.6. [Изучить инструкцию по охране труда программиста.](./1.6/1.6.md)

1.7. [Изучить "Квалификационный справочник должностей руководителей, специалистов и других служащих" (утв. Постановлением Минтруда России от 21.08.1998 N 37) (ред. от 12.02.2014) Инженер-программист (программист).](./1.7/1.7.md)

1.8. [Проанализировать справочную систему «Охрана труда» http://vip.1otruda.ru/#/document/16/22020/bssPhr1/?of=copy-063d39f27a.](./1.8/1.8.md)

1.9. [Изучить Постановление Главного государственного санитарного врача РФ от 21.06.2016 N 81 "Об утверждении СанПиН 2.2.4.3359-16 "Санитарно-эпидемиологические требования к физическим факторам на рабочих местах" (вместе с "СанПиН 2.2.4.3359-16. Санитарно-эпидемиологические правила и нормативы...") (Зарегистрировано в Минюсте России 08.08.2016 N 43153).](./1.9/1.9.md)

1.10. [Провести инсталляцию программного обеспечения.](./1.10/1.10.md)

1.11. [Изучить и проанализировать аппаратное, программное и информационное обеспечение автоматизированного рабочего места специалиста в конкретной предметной области (по выбору студента). Оценка рабочего места специалиста.](./1.11/1.11.md)

## Вариативные задания

2.1. [Сделать описание рабочего места программиста.](./2.1/2.1.md)

2.2. [Разработать инструкцию «Первая медицинская помощь при электротравме на рабочем месте программиста».](./2.2/2.2.md)

2.3. [Изучить прикладное программное обеспечение информационно-вычислительной системы предприятия ](./2.3/2.3.md)

2.4. [Изучить технические средства информационно-вычислительной системы предприятия (организации).](./2.4/2.4.md)
